'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Persona = Schema({
    nombre: String,
    edad: {
        type:Number,
        min:10,
        max:90
    },
    sexo: {
        type: String,
        enum: ['H', 'M', 'No binario']
    },
    codigo:String
});

module.exports = mongoose.model('Persona', Persona);
