var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config')

var cors = require('cors')

var app=express();

//cargar las rutas
var person_routes=require('./routes/person');

//history
var history = require('connect-history-api-fallback');


// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }))
 
// parse application/json
app.use(express.json())

//configurar cabeceras http
app.use(cors());

app.set('llave', config.llave);

//rutas base
app.use('/api', person_routes);

//history
app.use(history());

//carpeta publica
app.use(express.static(__dirname + '/public/'));


module.exports=app;