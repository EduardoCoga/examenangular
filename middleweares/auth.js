const jwt = require('jsonwebtoken');
const app = require('../app');
const config = require('../config')

exports.auth = function (req, res, next) {
    const token = req.headers['access-token'];    
    if (token) {
        jwt.verify(token, config.llave, { ignoreExpiration: true } , (err, decoded) => {
            if (err) {
                return res.json('Token inválida');
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.json('Token no proveída.');
    }
}