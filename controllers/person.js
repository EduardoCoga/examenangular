'use strict'
var jwt = require('jsonwebtoken');
var config = require('../config')

var PersonSchema = require('../models/person');

async function createPerson(req, res) {
    var body = req.body;    
    try {
        const person = await PersonSchema.create(body);
        res.status(200).json(person);
    } catch (error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error',
            error: error
        })
    }

}

async function getPerson(req, res) {
    var _id = req.params.id;
    var person;
    try {
        person = await PersonSchema.findById(_id);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'ID erroneo',
            error: error
        })
    }
    try {
        if (!person) {
            return res.status(404).json({
                mensaje: 'No se encontró la persona que buscaba',
                error
            })
        }
        res.json(person);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error: error
        })
    }
}

async function deletePerson(req, res) {
    var _id = req.params.id;
    var del;
    try {
        del = await PersonSchema.findByIdAndDelete(_id);
        if (del) {
            res.status(200).json(del);
        } else {
            res.status(404).json({ error: 'No se encontro la persona' });
        }
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error: error
        })
    }
}

async function getPeople(req, res) {
    const people = await PersonSchema.find();
    if (people) {
        res.status(200).json(people);
    } else {
        res.status(400).send({ message: "No hay personas registradas" });
    }

}

async function editPerson(req, res) {
    const _id = req.params.id;
    const update = req.body;
    const edit = await PersonSchema.findByIdAndUpdate(_id, update, { new: true });
    //const edit = await PersonSchema.findOneAndUpdate({_id:_id},update);
    if (edit) {
        res.status(200).json("Actualización exitosa");
    } else {
        res.status(404).json({
            message: "Ocurrio un error",
            edit
        })
    }
}

function test(req, res) {
    res.send('hello world');
}

function logIn(req, res) {
    var body = req.body;    
    if(body.user == "eduardo@coga.com" && body.password == "eduardo123"){
        const payload = {
            check: true
        };
        const token = jwt.sign(payload, config.llave, {
            expiresIn: "10h"
        });
        res.status(200).json({
            user:body.user,
            accesToken:token
        });
    }else{
        res.status(404).json({
            message: "Datos incorrectos"
        })
    }

}

module.exports = {
    getPerson,
    createPerson,
    deletePerson,
    getPeople,
    editPerson,
    test,
    logIn
}