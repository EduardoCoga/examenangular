const mongoose = require('mongoose');
var app = require('./app.js')
var config = require('./config')
var port = process.env.PORT || config.port;

//URI
const uri = config.uri2;
const options = {
  useNewUrlParser: true, 
  useCreateIndex: true, 
  useUnifiedTopology: true, 
  useFindAndModify: false
};

//connection to BD
mongoose.connect(uri, options).then(
  () => { 
    console.log("Conectado a la BD")    
   },
  err => { 
    console.log(err)
   }
);

app.listen(port, function () {
    console.log('Servidor web escuchando en el puerto ' + port);
});