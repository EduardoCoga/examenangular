'use strict'

var express = require('express');
var personController = require('../controllers/person');
var api = express.Router();
var auth = require('../middleweares/auth');

api.get('/getPerson/:id', auth.auth, personController.getPerson);
api.post('/createPerson/', auth.auth, personController.createPerson);
api.delete('/deletePerson/:id', auth.auth, personController.deletePerson);
api.get('/getPeople/', auth.auth, personController.getPeople);
api.put('/editPerson/:id', auth.auth, personController.editPerson);
api.get('/test/',auth.auth, personController.test)
api.post('/logIn/',personController.logIn)

module.exports = api;